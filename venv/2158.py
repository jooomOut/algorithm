import heapq, math

def find(data, v, row, col):
    dist = [[math.inf for _ in range(col+1)] for _ in range(row+1)]
    dist[0][0] = 0
    q = []
    heapq.heappush(q, [0, 0, 0, v])

    while q:
        # 다음 위치 찾기
        cost, hereX, hereY, v = heapq.heappop(q)

        # 새로 찾은 위치의 코스트 합리성 체크
        # 항상 바로 가는 것과 돌아가는 것을 비교하면 된다.
        if cost > dist[hereX][hereY]:
            continue

        nextCost = 1 / v + dist[hereX][hereY]
        ways = [[1, 0], [-1, 0], [0, 1], [0, -1]]
        for x,y in ways:
            if 0 <= hereX + x <= row and 0 <= hereY + y <= col:
                if nextCost < dist[hereX+x][hereY+y]:
                    dist[hereX+x][hereY+y] = nextCost
                    newVelo = v * 2**(data[hereX][hereY] - data[hereX+x][hereY+y])
                    heapq.heappush(q, [nextCost, hereX +x , hereY + y, newVelo])

    return dist

def main():
    velocity, row, col = map(int, input().split())

    data = []
    for _ in range(row):
        data.append(list(map(int, input().split())))

    # 0,0 부터 row-1, col-1까지

    ret = find(data, velocity, row-1, col-1)

    print(ret[row-1][col-1])

main()