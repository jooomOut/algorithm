n = int(input())
data = list(map(int,input().split()))

for i in range(1,n) :
    data[i] = data[i] + data[i-1]

ret = sum(data)
ret = ret - data[n-1]

print(ret)