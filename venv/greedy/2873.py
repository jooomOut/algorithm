import sys
r, c = map(int, input().split())
data = []
for i in range(r):
    data.append(list(map(int, input().split())))
ret = ''
if r % 2 == 1:
    for row in range(r):
        if row % 2 == 0:
            ret = ret + 'R'*(c-1)
        else:
            ret = ret + 'L' * (c - 1)
        if row != r - 1:
            ret = ret + 'D'

else :
    if c % 2 ==1 :
        for col in range(c):
            if col % 2 == 0:
                ret = ret + 'D' * (r - 1)
            else:
                ret = ret + 'U' * (r - 1)

            if col != c - 1:
                ret = ret + 'R'

    else :

        endRet = ''
        minC = sys.maxsize
        minR = sys.maxsize
        minData = sys.maxsize
        for row in range(r):
            for col in range(c):
                if (col + row) % 2 == 1:
                    if minData >= data[row][col] :
                        minData = data[row][col]
                        minR = row
                        minC = col


        start = 0
        end = r-1
        while start +1 < minR :
            ret = ret + 'R'*(c-1) + 'D' + 'L'*(c-1) + 'D'
            start += 2
        while end -1 > minR :
            endRet =  'D'  + 'L' * (c - 1) + 'D' + 'R' * (c - 1) + endRet
            end -= 2
        temp = ''
        desR = start + 1
        # start -> end
        nowCol = 0
        while start != desR or nowCol != c-1 :
            if minC == nowCol + 1 and start == minR:
                temp = temp + 'DR'
                start += 1
                nowCol += 1
            elif minR == start + 1 and minC == nowCol:
                temp = temp + 'RD'
                start += 1
                nowCol += 1
            else :
                if start != desR :
                    temp = temp + 'DRUR'
                    nowCol += 2
                else :
                    temp = temp + 'RURD'
                    nowCol += 2

        ret = ret + temp + endRet

print(ret)