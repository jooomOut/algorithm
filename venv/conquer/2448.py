import  sys
def tri(data,topCol, topRow, col, row, center ) :
    if center is False :
        if col == 3 :
            data[topCol][topRow] = "*"

            data[topCol+1][topRow-1] = "*"
            data[topCol+1][topRow+1] = "*"

            data[topCol+2][topRow-2] = "*"
            data[topCol + 2][topRow - 1] = "*"
            data[topCol + 2][topRow] = "*"
            data[topCol + 2][topRow + 1] = "*"
            data[topCol + 2][topRow + 2] = "*"
        else :
            tri(data, topCol, topRow, col//2, row//2, False)

            tri(data, topCol + col//2, topRow - row //4-1,  col // 2, row // 2, False)
            tri(data, topCol+ col//2 , topRow          ,  col // 2,  row // 2, True)
            tri(data, topCol+ col//2 , topRow + row // 4+1, col // 2, row // 2, False)

col = int(input())
row = 2*col -1
data = [[" " for _ in range(row)] for _ in range(col)]


tri(data,0, row//2, col, row , False)
for i in range(col):
    for j in range(row):
        sys.stdout.write(data[i][j])
    print()

