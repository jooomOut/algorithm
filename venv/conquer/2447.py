import sys
def star(data, y, x, n, center) :
    if center :
        for i in range(y, y+n) :
            for j in range(x, x+n) :
                data[i][j] = " "
    elif n > 3 :
        inv = n // 3
        star(data, y, x, inv, False)
        star(data, y, x + inv, inv, False)
        star(data, y, x + 2*inv, inv, False)

        star(data, y + inv, x, inv, False)
        star(data, y + inv, x + inv, inv, True)
        star(data, y + inv, x + 2*inv, inv, False)

        star(data, y + 2*inv, x, inv, False)
        star(data, y + 2*inv, x + inv, inv, False)
        star(data, y + 2*inv, x + 2*inv, inv, False)
    else :
        data[y][x] = "*"
        data[y][x+1] = "*"
        data[y][x+2] = "*"

        data[y+1][x] = "*"
        data[y+1][x+1] = " "
        data[y+1][x+2] = "*"

        data[y+2][x] = "*"
        data[y+2][x+1] = "*"
        data[y+2][x+2] = "*"


n = int(input())

data = [[' ' for _ in range(n)] for _ in range(n)]

star(data, 0, 0, n, False)

for i in range(n):
    for j in range(n):
        sys.stdout.write(data[i][j])
    print()