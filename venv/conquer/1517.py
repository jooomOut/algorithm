def mergeSort(data, left, right) :
    mid = (left + right) // 2
    if left < right :
        mergeSort(data, left, mid)
        mergeSort(data, mid+1, right)
        merge(data, left, mid, right)

def merge(data, left, mid, right) :
    global ret, ans
    cnt = 0
    i = left
    j = mid + 1
    k = left
    while i <= mid and j <= right :
        if data[i] <= data[j] :
            ret[k] = data[i]
            i += 1
            k += 1
            ans = ans + cnt
        else :
            ret[k] = data[j]
            j += 1
            k += 1
            cnt = cnt + 1
    if i > mid :
        for p in range(j,right+1) :
            ret[k] = data[p]
            k += 1
    else :
        for p in range(i,mid+1) :
            ret[k] = data[p]
            k += 1
            ans = ans + cnt

    for p in range(left, k) :
        data[p] = ret[p]

n = int(input())
data = list(map(int,input().split()))
ans = 0
ret = [-1 for _ in range(1000000)]
mergeSort(data, 0, len(data)-1)
print(ans)