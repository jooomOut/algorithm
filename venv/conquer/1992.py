def zip(data, y, x, length, result) :

    cnt = 0
    for i in range(y,y+length) :
        for j in range(x, x + length):
            cnt = cnt + int(data[i][j])
    if cnt == 0 :
        result.append(str(0))
    elif cnt == length*length :
        result.append(str(1))
    else :
        cnt = -1
    if cnt == -1 and length > 1 :
        result.append('(')
        zip(data, y,          x,          length//2, result)
        zip(data, y,          x+length//2, length//2, result)
        zip(data, y+length//2, x,          length//2, result)
        zip(data, y+length//2, x+length//2, length//2, result)
        result.append(')')


n = int(input())
data = []
for _ in range(n) :
    data.append(list(input()))

result = []

zip(data,0, 0, n, result)

print("".join(result))


