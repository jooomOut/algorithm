t = int(input())

maxDp = [[0 for _ in range(3)] for _ in range(2)]
minDp = [[0 for _ in range(3)] for _ in range(2)]
data = list(map(int, input().split()))
maxDp[0] = data.copy()
minDp[0] = data.copy()
for _ in range(t-1):
    data = list(map(int, input().split()))

    maxDp[1][0] = max(data[0] + maxDp[0][0],
                        data[0] + maxDp[0][1])
    maxDp[1][1] = max(data[1] + maxDp[0][0],
                    data[1] + maxDp[0][1],
                    data[1] + maxDp[0][2])
    maxDp[1][2] = max(data[2] + maxDp[0][1],
                        data[2] + maxDp[0][2])

    maxDp[0][0] = maxDp[1][0]
    maxDp[0][1] = maxDp[1][1]
    maxDp[0][2] = maxDp[1][2]


    minDp[1][0] = min(data[0] + minDp[0][0],
                      data[0] + minDp[0][1])
    minDp[1][1] = min(data[1] + minDp[0][0],
                      data[1] + minDp[0][1],
                      data[1] + minDp[0][2])
    minDp[1][2] = min(data[2] + minDp[0][1],
                      data[2] + minDp[0][2])

    minDp[0][0] = minDp[1][0]
    minDp[0][1] = minDp[1][1]
    minDp[0][2] = minDp[1][2]

print("%d %d" % (max(maxDp[0]), min(minDp[0])))
