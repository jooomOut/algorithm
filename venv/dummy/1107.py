target = int(input())
wrongNum = int(input())
if wrongNum != 0:
    breakButtons = list(map(str, input().split()))
else:
    breakButtons = []

diff = 0
moveTo = 100
if target == 100:
    print(0)
    exit(0)
if wrongNum == 10:
    if target >= 100:
        print(target-100)
    else:
        print(100-target)
    exit()
elif wrongNum == 0:
    if target >= 100:
        moves = target - 100
    else:
        moves = 100 -target
    ret = min(moves, len(str(target)))
    print(ret)
    exit()

## TODO 가까운 수 찾기가 먼저야.

while True:
    error = False
    if target - diff >= 0:
        string = str(target - diff)
        for x in string:
            if x in breakButtons:
                error = True
                break

        if error is False:
            moveTo = target - diff
            break

        if target - diff == 100:
            print(target - 100)
            exit()

    ####################################
    error = False
    string = str(target + diff)
    for x in string:
        if x in breakButtons:
            error = True
            break

    if error is False:
        moveTo = target + diff
        break
    if target - diff == 100:
        print(target - 100)
        exit()


    diff += 1


if target > moveTo:
    clicks = target - moveTo
else:
    clicks = moveTo - target
moves = None
if moveTo == 100:
    moves = 0
else:
    moves = len(str(moveTo))

fromOrigin = target - 100
if fromOrigin < 0:
    fromOrigin = -fromOrigin
if fromOrigin < clicks + moves:
    print(fromOrigin)
else:
    print(clicks + moves)