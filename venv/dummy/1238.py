import heapq


def dij(start, dest):
    if start == dest:
        return 0
    dist = [INF] * (n + 1)
    dist[start] = 0
    q = []
    heapq.heappush(q, [0, start])

    while q:
        cost, here = heapq.heappop(q)
        if dist[here] < cost:
            continue

        for next in data[here].keys():
            nextCost = cost + data[here][next]
            if nextCost < dist[next]:
                dist[next] = nextCost
                heapq.heappush(q, [nextCost, next])



    return dist[dest]


n, m, x = map(int, input().split())
data = {}
for _ in range(m):
    u, v, co = map(int, input().split())
    if u not in data.keys():
        data[u] = {}
    data[u][v] = co

INF = int(1e9)
ret = -1
for i in range(n):
    ret = max(ret, dij(i+1, x) + dij(x, i+1))

print(ret)