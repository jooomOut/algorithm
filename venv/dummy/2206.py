from collections import deque
import math


def canReach(x,y):
    if 0 <= x < n and 0 <= y < m:
        return True
    return False

def bfs(data, dp, visited):
    q = deque()
    q.append([0,0,0])
    visited[0][0][0] = visited[0][0][1] = 1
    move = [[1, 0], [-1, 0], [0, 1], [0, -1]]
    dp[0][0] = 1

    while q:
        x, y, wall = q.popleft()
        for moveX, moveY in move:
            nextX = x + moveX
            nextY = y + moveY
            if canReach(nextX, nextY) is True:
                if data[nextX][nextY] == 1 and wall == 0 and visited[nextX][nextY][wall + 1] == 0:
                    visited[nextX][nextY][wall +1] = 1
                    dp[nextX][nextY] = dp[x][y] + 1
                    q.append([nextX, nextY, wall + 1])

                if data[nextX][nextY] == 0 and visited[nextX][nextY][wall] == 0:
                    visited[nextX][nextY][wall] = 1
                    dp[nextX][nextY] = dp[x][y] + 1
                    q.append([nextX,nextY,wall])


                if nextX == n - 1 and nextY == m - 1:
                    print(dp[nextX][nextY])
                    return

    print(-1)





n , m = map(int, input().split())
data = []
dp = []
visited= []
for _ in range(n):
    dp.append([0] * m)
    visited.append([[0,0] for _ in range(m)])
    temp = []
    line = input()
    for x in line:
        temp.append(int(x))
    data.append(temp)
if n ==1 and m == 1:
    print(1)
else:
    bfs(data, dp, visited)

