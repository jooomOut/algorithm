a = ' '+input()
b = ' '+input()

sizeA = len(a)
sizeB = len(b)
dp = [[0 for _ in range(sizeA)]  for _ in range(sizeB)]


for i in range(1, sizeB): # b
    for j in range(1, sizeA): # a
        if a[j] == b[i]:
            dp[i][j] = dp[i-1][j-1] + 1
        else:
            dp[i][j] = max(dp[i][j - 1], dp[i - 1][j])

print(dp[sizeB-1][sizeA-1])