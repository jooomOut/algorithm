from collections import deque
import sys

def getNext(start, command):
    if command == 'D':
        ret = (start * 2) % 10000
    elif command == 'S':
        ret = start
        ret -= 1
        if ret == -1:
            ret = 9999
    elif command == 'L':
        left = start // 1000
        ret = ((start % 1000) * 10) + left
    else:
        right = start % 10
        ret = right * 1000 + start // 10
    return ret


def search(start, end):
    visited = [-1 for _ in range(10000)]
    cal = ['D', 'S', 'L', 'R']
    cache = {}
    q = deque()

    q.append(start)
    # D S L R
    try:
        while q:
            curr = q.pop()

            for x in cal:
                next = getNext(curr, x)

                if next == end:
                    trace = []
                    trace.append(x)

                    while True:
                        try:
                            number, command = cache[curr]
                        except KeyError:
                            print(x)
                            raise KeyError

                        trace.append(command)
                        curr = number
                        if curr == start:
                            trace.reverse()
                            sys.stdout.write("".join(trace))
                            print()
                            raise KeyError


                if visited[next] == -1:
                    q.appendleft(next)
                    cache[next] = [curr, x]
                    visited[next] = 1
    except KeyError:
        pass









t = int(input())
for _ in range(t):
    start, end = map(int, input().split())


    search(start, end)