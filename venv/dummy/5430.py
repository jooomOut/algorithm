import sys, json
def Delete(start, last, asc):# 에러는 여기서 난다
    if asc:
        start += 1
    else :
        last -= 1

    return start, last


t = int(input())

for _ in range(t):
    func = input()
    n = int(input())
    data = input()
    data = json.loads(data)

    start = 0
    last = len(data) - 1
    asc = True
    isError = False
    for x in func:
        if x == 'R':
            # TODO 뒤집기
            if asc:
                asc = False
            else:
                asc = True
        else:
            # 앞에서 버리지
            start, last = Delete(start, last, asc)
            n -= 1
            if n == -1:
                print("error")
                isError = True
                break

    if isError is False:
        if asc:
            ret = str(data[start:last+1])
            for x in ret:
                if x != ' ':
                    print(x, end='')
            #sys.stdout.write("".join(ret))
            print()

        else:
            ret = data[start:last+1]
            ret.reverse()
            ret = str(ret)
            for x in ret:
                if x != ' ':
                    print(x, end='')
            print()
