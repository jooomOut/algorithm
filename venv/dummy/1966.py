t = int(input())

for _ in range(t):
    n, target = map(int, input().split())
    data = list(map(int, input().split()))

    ret = 0
    length = len(data)
    ourMax = max(data)

    nextUp = 0
    for i in range(length):
        if ourMax >= data[i] > data[target]:
            nextUp = i
            ourMax = data[i]

    for i in range(0, target):
        if data[i] > data[target]:
            ret += 1
    for i in range(target+1, length):
        if data[i] > data[target]:
            ret += 1

    if nextUp < target:
        for i in range(nextUp, target):
            if data[i] == data[target]:
                ret += 1
    else :
        for i in range(0, target):
            if data[i] == data[target]:
                ret += 1
        for i in range(nextUp+1, length):
            if data[i] == data[target]:
                ret += 1

    print(ret+1)

# 찾는 값이 MAX가 아닐 때 ?  다음 큰 값의 마지막 부터~