import math
def Floyd(data):
    for k in range(1, n + 1):
        for i in range(1, n+1):
            for j in range(1, n+1):
                data[i][j] = min(
                    data[i][j],
                    data[i][k] + data[k][j],
                )


n = int(input())
b = int(input())
data =[[math.inf for _ in range(n+1)] for _ in range(n+1)]
for _ in range(b):
    a, b, c = map(int, input().split())
    if c < data[a][b]:
        data[a][b] = c

for i in range(n+1):
    data[i][i] = 0

Floyd(data)
for i in range(1, n + 1):
    for j in range(1, n + 1):
        if data[i][j] != math.inf:
            print(data[i][j], end=' ')
        else:
            print(0, end=' ')
    print()