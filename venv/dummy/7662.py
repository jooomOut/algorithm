import heapq

t = int(input())

for _ in range(t):
    n = int(input())
    maxH = []
    minH = []
    id = 0
    cache = {}
    for _ in range(n):
        command, data = map(str, input().split())
        data = int(data)

        if command == 'I':
            heapq.heappush(maxH, [-data, id])
            heapq.heappush(minH, [data, id])

            try:
                cache[id] += 1
            except KeyError:
                cache[id] = 1

            id += 1

        elif command == 'D':
            if data == 1:
                popId = None
                if maxH:
                    popData, popId = heapq.heappop(maxH)
                while maxH and cache[popId] == 0:
                    popData, popId = heapq.heappop(maxH)
                try:
                    if cache[popId] != 0:
                        cache[popId] -= 1
                except KeyError:
                    pass
            else:
                popId = None
                if minH:
                    popData, popId = heapq.heappop(minH)
                while minH and cache[popId] == 0:
                    popData, popId = heapq.heappop(minH)
                try:
                    if cache[popId] != 0:
                        cache[popId] -= 1
                except KeyError:
                    pass


    maxData = None
    minData = None
    while maxH:
        popData, popId = heapq.heappop(maxH)
        if cache[popId] == 0:
            continue
        else:
            maxData = -popData
            break

    while minH:
        popData, popId = heapq.heappop(minH)
        if cache[popId] == 0:
            continue
        else:
            minData = popData
            break

    if maxData is None and minData is None:
        print("EMPTY")
        continue
    else:
        print("%d %d" %(maxData, minData))



