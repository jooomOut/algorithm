# https://www.acmicpc.net/problem/1874

n = int(input())
data = []
number = []
stack = []
ret = []
for i in range(n):
    data.append(int(input()))
    number.append(n-i)

idx = 0
for i in range(n):
    while True:
        if number:
            last = number.pop()
            if last == data[i]:
                ret.append('+')
                ret.append('-')
                break
            elif last < data[i]:
                stack.append(last)
                ret.append('+')
                continue
            else:
                number.append(last)
                forStack = stack.pop()
                if forStack != data[i]:
                    print("NO")
                    exit(0)
                else:
                    ret.append("-")
                    break

        else:
            if stack:
                forStack = stack.pop()
                if forStack != data[i]:
                    print("NO")
                    exit(0)
                else:
                    ret.append("-")
            break

for x in ret:
    print(x)

