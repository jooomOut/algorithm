from collections import deque


def isCorner(x, y):
    nList = [[1, 0], [-1, 0], [0, 1], [0, -1]]
    count = 0
    for nx, ny in nList:
        nextX = x + nx
        nextY = y + ny
        if 0 <= nextX < n and 0 <= nextY < m:
            if data[x + nx][y + ny] == -1:
                count += 1

    if count >= 2:
        return True
    return False


def clearAir(data):
    q = deque()
    q.append([0, 0])
    nList = [[1, 0], [-1, 0], [0, 1], [0, -1]]
    visited = [[0 for _ in range(m)] for _ in range(n)]
    while q:
        x, y = q.popleft()
        visited[x][y] = 1
        for nx, ny in nList:
            nextX = x + nx
            nextY = y + ny
            if 0 <= nextX < n and 0 <= nextY < m:
                if visited[nextX][nextY] == 0 and data[nextX][nextY] != 1 and [nextX, nextY] not in q:
                    q.append([x + nx, y + ny])
                    data[x + nx][y + ny] = -1


n, m = map(int, input().split())
data = []

for _ in range(n):
    data.append(list(map(int, input().split())))

ret = 0

while True:
    # TODO: air 찾기
    clearAir(data)
    target = []
    for x in range(n):
        for y in range(m):
            if data[x][y] == 1 and isCorner(x, y) is True:
                target.append([x, y])

    for x, y in target:
        data[x][y] = 0
    ret += 1

    if len(target) == 0:
        break

print(ret-1)
