import sys
import math
def getDistance(x1, y1, x2, y2):
    return math.sqrt( math.pow(x2 - x1, 2) + math.pow(y2 - y1, 2) )

Ax, Ay, Bx, By, Cx, Cy, Dx, Dy = map(int,input().split())
interval = 1000000
speed_m_x = (Bx - Ax) / interval
speed_m_y = (By - Bx) / interval
speed_k_x = (Dx - Cx) / interval
speed_k_y = (Dy - Cy) / interval

distance = getDistance(Ax, Ay, Cx, Cy)

for i in range(interval) :
    distance = min(distance ,
                   getDistance(Ax+i*speed_m_x, Ay+i*speed_m_y, Cx+i*speed_k_x, Cy+i*speed_k_y))

print(distance)