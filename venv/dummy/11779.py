import heapq, math, sys

def find(start, end):
    q = []
    dist = [math.inf for _ in range(n+1)]
    dist[start] = 0

    q.append([0, start])
    while q:
        cost, here = heapq.heappop(q)

        if dist[here] < cost:
            continue

        for next in data[here].keys():
            nextCost = dist[here] + data[here][next]
            if dist[next] > nextCost:
                dist[next] = nextCost
                heapq.heappush(q, [nextCost, next])
                prev[next] = here

    return dist[end]

def findRoute(start, end):
    route = []
    now = end
    route.append(end)
    count = 1
    while now != start:
        route.append(prev[now])
        count += 1
        now = prev[now]

    print(count)
    route.reverse()
    for x in route:
        print("%d " %x, end='')
    #sys.stdout.write(" ".join(route).strip())


n = int(input())
m = int(input())
prev = {}
data = {}
for i in range(1, n+1):
    data[i] = {}
    prev[i] = {}
for _ in range(m):
    start, end, cost = map(int, input().split())
    if end not in data[start].keys() or cost < data[start][end]:
        data[start][end] = cost

start, end = map(int, input().split())

print(find(start, end))

findRoute(start,end)
