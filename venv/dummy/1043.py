def find(x):
    if x == parent[x]:
        return x
    else:
        y = find(parent[x])
        parent[x] = y
        return y

def union(x, y):
    x = find(x)
    y = find(y)
    if x != y:
        parent[y] = x

n, m = map(int, input().split())

tr = list(map(int, input().split()))
trueNum = tr[0]
trueMan = []
for i in range(1, trueNum+1):
    trueMan.append(tr[i])
parties = []
for _ in range(m):
    parties.append(list(map(int, input().split()))[1:])

parent = [i for i in range(n+1)]

for i in range(m):
    party = parties[i]

    for j in range(len(party)-1):
        union(party[j],party[j+1])


cnt = m

for party in parties:
    for y in trueMan:
        if find(party[0]) == find(y):
            cnt -= 1
            break

print(cnt)





