import sys

sys.setrecursionlimit(1000000)

n = int(input())


def get(n):
    try:
        if dp[n]:
            return dp[n]
    except Exception:
        pass
    if n == 0:
        return 0
    elif n == 1 or n == 2:
        return 1

    if n % 2 == 0:
        m = n // 2
        t1 = get(m) % DIV
        t2 = get(m-1) % DIV
        ret = (t1 * (t1 + 2 * t2)) % DIV
        dp[n] = ret
        return ret
    else:
        m = (n + 1) // 2
        t1 = get(m) % DIV
        t2 = get(m-1) % DIV
        ret = (t1*t1 + t2*t2) % DIV
        dp[n] = ret
        return ret

DIV = 1000000007
dp = {}
print(get(n) % 1000000007)
