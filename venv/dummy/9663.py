def possible(targetRow, targetCol):
    global count
    for row in range(targetRow):
        if queen[row] == targetCol:
            return False
        if targetRow - row == targetCol - queen[row] or targetRow - row == queen[row] - targetCol:
            return False

    return True

def solve(row):
    global count
    if row == n:
        count += 1
        return

    for col in range(n):
        queen[row] = col
        if possible(row, col):
            solve(row+1)

    return

n = int(input())
queen = [-1 for _ in range(n)]
count = 0
solve(0)
print(count)