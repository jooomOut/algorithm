import sys

def findLeft(data, idx):
    if data[idx-1] == ')': # 바로 왼쪽이 닫는 괄호면 해당 괄호 너머에 여는 괄호 추가
        count = 1
        loop = idx - 2
        while count != 0:
            if data[loop] == ')':
                count +=1
            elif data[loop] == '(':
                count -= 1
            loop -= 1
        return loop + 1
    elif idx-2 >= 0 and data[idx-2] == '(':
        #return False
        return idx-2
    else:
        return idx-1
def findRight(data, idx):
    if idx > 0 and data[idx+1] == '(': # 바로 오른쪽이 여는 괄호면 해당 괄호 너머에 닫는 괄호 추가
        count = 1
        loop = idx + 2
        while count != 0:
            if data[loop] == '(':
                count += 1
            elif data[loop] == ')':
                count -= 1
            loop += 1
        return loop
    elif idx+2 < len(data) and data[idx+2] == ')':
        #return False
        return idx + 3
    else:
        return idx+2

def addBrackets(data):
    ret = []
    for x in data:
        ret.append(x)
    i = 0
    while i < len(ret):
        if ret[i] == '*' or ret[i] == '/':
            left = findLeft(ret, i) # 여는 괄호를 삽입할 위치
            right = findRight(ret, i) # 닫는 괄호를 삽입할 위
            if left is not False and right is not False:
                ret.insert(left, '(')
                ret.insert(right+1, ')')
                i+=2
        i+=1
    i = 0
    while i < len(ret):
        if ret[i] == '+' or ret[i] == '-':
            left = findLeft(ret, i)
            right = findRight(ret, i)
            if left is not False and right is not False:
                ret.insert(left, '(')
                ret.insert(right+1, ')')
                i += 2
        i += 1

    return ret


def solve2(bracket):
    s = []
    post = ''
    for x in bracket:
        if x not in oper and x not in brack:
            post = post + x
        elif x == ')':
            poped = s.pop()
            while s and poped != '(':
                if poped != '(':
                    post = post + poped
                poped = s.pop()

        else:
            s.append(x)
    return post


oper = ['+', '-', '*', '/']
brack = ['(', ')']

data = input().strip()
data = addBrackets(data)
sys.stdout.write("".join(data))
print()
data = solve2(data)
print(data)

