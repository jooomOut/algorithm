def find():
    dist = [10000* 3000 for _ in range(n + 1)]
    ret = True
    for i in range(1, n + 1):
        for j in range(1, n + 1):
            for next, weight in data[j]:
                if dist[next] > weight + dist[j]:
                    dist[next] = weight + dist[j]
                    if i == n:
                        ret = False
    return ret

tc = int(input())

for _ in range(tc):
    n, m, w = map(int, input().split())
    data = [[] for _ in range(n+1)]

    for _ in range(m):
        s, e, t = map(int, input().split())
        data[s].append([e, t])
        data[e].append([s, t])

    for _ in range(w):
        s, e, t = map(int, input().split())
        data[s].append([e, -t])

    print("NO" if find() else "YES")



