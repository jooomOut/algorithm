n = int(input())
cards = list(map(int, input().split()))
cards.sort()
m = int(input())
wish = list(map(int, input().split()))


for i in range(m) :
    isThere = False
    left = 0
    right = n-1
    while left <= right :
        mid = (left + right) // 2

        if wish[i] > cards[mid] :
            left = mid + 1
        elif wish[i] < cards[mid] :
            right = mid - 1
        else :
            isThere = True
            break
    if isThere :
        wish[i] = 1
    else :
        wish[i] = 0

for y in wish :
    print(y,end=' ')

