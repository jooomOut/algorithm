import sys
n, c = map(int, input().split())
data = []

for _ in range(n) :
    where =int(input())
    data.append(where)

data.sort()

ans = 0
right = data[len(data)-1]
left = 1
while left <= right :
    wid = (right+left) // 2
    install = 1
    before = data[0]
    for i in range(1, n) :
        if data[i] - before < wid :
            continue
        else :
            install += 1
            before = data[i]

    if install >= c :
        ans = wid
        left = wid+1
    else :
        right = wid-1
print(ans)

