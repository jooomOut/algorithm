import  bisect
def up(left, right, target) :
    while left < right :
        mid = (left + right) // 2

        if target >= cards[mid] :
            left = mid + 1
        else :
            right = mid
    return right
def down(left, right, target) :
    while left < right :
        mid = (left + right) // 2

        if target > cards[mid] :
            left = mid+1
        else :
            right = mid
    return right

n = int(input())
cards = list(map(int, input().split()))
cards.sort()
m = int(input())
wish = list(map(int, input().split()))

maxCard = max(cards)
for i in range(m) :
    left = 0
    right = n-1
    #upIdx = up(left,right,wish[i])
    #downIdx = down(left, right, wish[i])
    target = wish[i]
    lower = bisect.bisect_left(cards, target ,left,right)
    upper = bisect.bisect_right(cards, target ,left,right)
    if wish[i] == maxCard :
        upper +=1
    wish[i] = upper - lower

for x in wish :
    print(x,end=' ')




