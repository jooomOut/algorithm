n = int(input())
data = list(map(int, input().split()))

def solve(left, right, data) :
    if left == right :
        return 0
    mid = (right + left) // 2
    result = 0

    maxLeft = 0
    for i in range(left, mid + 1):
        maxLeft = max(maxLeft, data[i])

    maxRight = 0
    for i in range(mid + 1, right + 1):
        maxRight = max(maxRight, data[i])

    result = max( maxLeft + solve(mid+1,right, data),
                  maxRight + solve(left,mid, data))

    return result
print(solve(0,n-1,data))

