import sys
sys.setrecursionlimit(1000000)

def step(data, start, dp, check) :
    if dp[start] != -1 :
        return dp[start]
    if check[start] is True :
        return 0
    check[start] = True
    result = 1
    next = start + data[start]
    if check[next] is False :
        result = result + step(data, next, dp, check)
        dp[start] = result
        return dp[start]
    else :
        dp[start] = result
        return dp[start]

n = int(input())
data = [0] + list(map(int,input().split()))
dp = [-1 for _ in range(n+1)]
check = [False for _ in range(len(data)+1)]

max12 = max(step(data,1, dp, check), step(data,2, dp, check))
maxF = max(max12, step(data,3, dp, check))
print(maxF+1)


