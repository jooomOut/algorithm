n = int(input())

data = [[] for _ in range(n**n+1)]
points=[0]
nums = [0]
sumData = 0
temp = []
for i in range(n**n) :
    temp  = list(map(int,input().split()))
    points.append(temp[0])
    nums.append(temp[1])
    data[i+1].append(temp[2:])
sumV = 0
a = 0
b = 0
c = 0
d = 0
for i in range(1,100000001) :
    arr = []
    if nums[1] > a and data[1][0][a] == i :
        arr.append(points[1])
        a = a + 1
    if nums[2] > b and data[2][0][b] == i :
        arr.append(points[2])
        b = b + 1
    if nums[3] > c and data[3][0][c] == i :
        arr.append(points[3])
        c = c + 1
    if nums[4] > d and data[4][0][d] == i :
        arr.append(points[4])
        d = d + 1

    if arr :
        sumV = sumV + max(arr)

    if nums[1] < a and nums[2] < b and nums[3] < c and nums[4] < d :
        break
print(sumV)