def dfs(start, line, result) :
    result.append(start)
    end = True
    for i in range(1, len(line[start])) :
        if line[start][i] is True :
            dfs(i, line, result)
            result.pop()
            end = False

    if end is True :
        for x in result :
            print(getSkillFromIdx[x],end=' ')
        print()
        return 0

skill = list(map(str, input().split()))
getIdxFromSkill={}
getSkillFromIdx={}
idx = 1
for x in skill :
    getIdxFromSkill[x] = idx
    getSkillFromIdx[idx] = x
    idx = idx + 1

line = [[False for _ in range(len(skill)+1)] for _ in range(len(skill)+1)]

n = int(input())
for i in range(n) :
    a, b = map(str, input().split())
    line[getIdxFromSkill[a]][getIdxFromSkill[b]] = True

for i in range(1, len(skill)+1) :
    starting = True
    for j in range(1,len(skill)+1) :
        if line[j][i] is True : # i는 시작으로 쓸 수 없다.
            starting = False
            break
    if starting :
        result = []
        dfs(i, line,result)








