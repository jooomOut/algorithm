from collections import deque
def clearTree(tree) :
    tree[1][0] = -1
    q = deque()
    q.append(1)
    while q :
        now = q.popleft()
        for i in range(1, len(tree[now])):
            q.append(tree[now][i])
            target = tree[now][i]
            tree[target].remove(now)
            tree[target][0] = now

n = int(input())
tree = {}
for i in range(1, n+1) :
    tree[i] = [-1]
for _ in range(n-1):
    a, b = map(int,input().split())
    tree[a].append(b)
    tree[b].append(a)

clearTree(tree)

for i in range(2,n+1):

    print(tree[i][0])

