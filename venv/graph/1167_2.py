import sys
import heapq
def dik(data, start, n) :
    distances = data[start]
    distances = {node: float('inf') for node in data}
    distances[start] = 0
    heap = []
    heapq.heappush(heap, [distances[start], start])

    while heap :
        currentDistance, currentDestination = heapq.heappop(heap)  # 탐색 할 노드, 거리를 가져옴.

        if distances[currentDestination] < currentDistance:  # 기존에 있는 거리보다 길다면, 볼 필요도 없음
            continue

        for nextDestination, nextDistance in data[currentDestination].items():
            distance = currentDistance + nextDistance  # 해당 노드를 거쳐 갈 때 거리
            if distance < distances[nextDestination]:  # 알고 있는 거리 보다 작으면 갱신
                distances[nextDestination] = distance
                heapq.heappush(heap, [distance, nextDestination])  # 다음 인접 거리를 계산 하기 위해 큐에 삽입

    return distances



n = int(input())
data = {}
for _ in range(n) :
    oneInput = list(map(int,input().split()))
    length = len(oneInput)
    now = oneInput[0]
    data[now] = {}
    for idx in range(1, length, 2) :
        if oneInput[idx] == -1 :
            break
        else :
            data[now][oneInput[idx]] = oneInput[idx+1]

target = dik(data, 1,n)
target[0] = 0
next = 0
for i in range(len(target)) :
    if target[next] < target[i] :
        next = i

ret = dik(data, next,n)
m = 0
for x in ret :
    if m < ret[x] :
        m = ret[x]
print(m)

