import sys
class Tree :
    def __init__(self) :
        self.left = None
        self.right = None
        self.data = None
    def addData(self, data):
        self.data = data
    def addLeft(self, data) :
        node = Tree()
        node.addData(data)
        self.left = node
    def addRight(self, data):
        node = Tree()
        node.addData(data)
        self.right = node
    def search(self, data):
        ret = -1
        if self.data == data :
            ret =  self
        else :
            if self.left is not None and ret == -1:
                ret = self.left.search(data)
            if self.right is not None and ret == -1:
                ret = self.right.search(data)
        return ret


    def preorder(self):
        sys.stdout.write(self.data)
        if self.left is not None :
            self.left.preorder()
        if self.right is not None:
            self.right.preorder()
    def inorder(self):
        if self.left is not None:
            self.left.inorder()
        sys.stdout.write(self.data)
        if self.right is not None:
            self.right.inorder()
    def postorder(self):
        if self.left is not None:
            self.left.postorder()
        if self.right is not None:
            self.right.postorder()
        sys.stdout.write(self.data)

n = int(input())
tree = Tree()
root, left, right = map(str, input().split())
tree.data = root
if left != '.' :
    tree.addLeft(left)
if right != '.' :
    tree.addRight(right)
for _ in range(n-1) :
    targetData, left, right = map(str, input().split())
    target = tree.search(targetData)
    if left != '.':
        target.addLeft(left)
    if right != '.':
        target.addRight(right)

tree.preorder()
print()
tree.inorder()
print()
tree.postorder()