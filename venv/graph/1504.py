import heapq,math

def find(start):
    dist = [math.inf for _ in range(n+1)]
    dist[start] = 0

    q = []
    q.append([0, start])
    while q:
        cost, here = heapq.heappop(q)

        if cost > dist[here]:
            continue

        for next in data[here].keys():
            nextCost = data[here][next] + dist[here]
            if nextCost < dist[next]:
                dist[next] = nextCost
                heapq.heappush(q, [nextCost, next])

    return dist




n, e = map(int, input().split())

data = {}
for i in range(1, n+1):
    data[i] = {}

for _ in range(e):
    s, end, c = map(int, input().split())
    data[s][end] = c
    data[end][s] = c

share_1, share_2 = map(int, input().split())

try:
    oneTo = find(1)
    middle = find(share_1)
    nTo = find(n)
    ret = min(
        oneTo[share_1] + middle[share_2] + nTo[share_2],
        oneTo[share_2] + middle[share_2] + nTo[share_1]
    )
    if ret != math.inf:
        print(ret)
    else:
        print(-1)
except:
    print(-1)