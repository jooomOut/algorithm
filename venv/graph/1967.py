import heapq
def search(tree, start, n) :
    distances = {i : float('inf') for i in range(1,n+1)}
    distances[start] = 0
    q = []
    heapq.heappush(q, [start, distances[start]])

    while q:
        nowNode, disToNowNode = heapq.heappop(q)

        # pass
        if distances[nowNode] < disToNowNode :
            continue

        for nextNode, nextDis in tree[nowNode].items() :
            distance = nextDis + disToNowNode
            if distance < distances[nextNode] :
                distances[nextNode] = distance
                heapq.heappush(q, [nextNode, distance])


    return distances


n = int(input())
tree ={}
for i in range(1,n+1) :
    tree[i] = {}
for _ in range(n-1) :
    parent, child, weight = map(int,input().split())
    tree[parent][child] = weight
    tree[child][parent] = weight

dis = search(tree,1, n)
idx = 1
for x in dis :
    if dis[idx] < dis[x] :
        idx = x

dis = search(tree, idx , n)
for x in dis :
    if dis[idx] < dis[x] :
        idx = x
print(dis[idx])

