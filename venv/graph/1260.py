from queue import Queue

def dfs(data, start, visited, stack) :
    print(start, end=' ')
    #여기 왔다
    visited[start] = True

    for i in range(n,0,-1) :
        if data[start][i] and visited[i] is False:
            stack.append(i)
    while len(stack) != 0 :
        next = stack.pop()
        if visited[next] is False :
            dfs(data, next, visited, stack)

def bfs(data, start, visited, queue) :
    print(start, end=' ')
    # 여기 왔다
    visited[start] = True

    for i in range(1, n + 1):
        if data[start][i] and visited[i] is False:
            queue.put(i)
            visited[i] = True


    while queue.qsize() != 0 :
        start = queue.get()
        print(start,end=' ')
        for i in range(1, n+1) :
            if data[start][i] and visited[i] is False :
                queue.put(i)
                visited[i] = True






n,m,start = map(int, input().split())

data = [[False for _ in range(n+1)] for _ in range(n+1)]
for i in range(m) :
    a, b = map(int, input().split())
    data[a][b] = True
    data[b][a] = True

visited = [False for _ in range(n+1)]
stack = []
dfs(data, start, visited, stack)
print()

queue = Queue()
visited = [False for _ in range(n+1)]
bfs(data, start, visited, queue)


