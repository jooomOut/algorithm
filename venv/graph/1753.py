import heapq, math

def find(data, start):
    dist = [math.inf for _ in range(v+1)]
    dist[start] = 0
    q = []
    heapq.heappush(q, (0, start))

    while q:
        cost, here = heapq.heappop(q)

        for next in data[here].keys():
            if dist[next] < data[here][next]:
                continue

            costSum = dist[here] + data[here][next]
            if costSum < dist[next]:
                dist[next] = costSum
                heapq.heappush(q, [costSum, next])


    return dist








v, e = map(int, input().split())
start = int(input())

data = {}
for i in range(1, v+1):
    data[i] = {}
for _ in range(e):
    u, u2, c = map(int, input().split())
    if u2 not in data[u].keys() or data[u][u2] > c:
        data[u][u2] = c


ret = find(data, start)
for i in range(1, v+1):
    if ret[i] == math.inf:
        print("INF")
    else:
        print(ret[i])

