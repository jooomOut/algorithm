import sys
#=================================================
size = int(sys.stdin.readline())
data = [0,0,0]
for i in range(size) :
    data.append(int(sys.stdin.readline()))
dp = [0 for _ in range(size+3)]
#=================================================

for j in range(3, size+3) :
    dp[j] = max(dp[j-3] + data[j] + data[j-1], dp[j-2] + data[j])
    dp[j] = max(dp[j], dp[j-1]) #중간데 두 번 연속 안먹는 상황도 있



#======================
print(dp[size+2])
