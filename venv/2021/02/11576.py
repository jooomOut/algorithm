def solve(a, b, data) :
    result = 0
    for i in range(len(data)-1, -1, -1) :
        result = result + int(data[i]) * a**(len(data)-i-1)

    ret = ''
    while result != 0 :
        ret = str(result % b) + ret
        result = result // b
    print(ret)


a, b = map(int,(input().split()))
n = int(input())

data = list(map(int,input().split()))

for i in range(len(data)) :
    solve(a,b,str(data[i]))
