from collections import deque
import sys


def bfs(line, visited, start) :
    q = deque()
    q.append(start)
    #visited[1] = 'W'
    while q :
        here = q.popleft()

        for x in line[here]:
            if visited[here] == 'W' :
                if visited[x] == 'W' :
                    return False
                elif visited[x] == 'N' :
                    q.append(x)
                    visited[x] = 'B'

            else :
                if visited[x] == 'B':
                    return False
                elif visited[x] == 'N':
                    q.append(x)
                    visited[x] = 'W'
    return True




tc = int(sys.stdin.readline())
for i in range(tc) :
    n, e = map(int, sys.stdin.readline().split())
    line = [[] for _ in range(n+1)]

    for j in range(e) :
        u, v = map(int, sys.stdin.readline().split())
        line[u].append(v)
        line[v].append(u)

    visited = ['N' for _ in range(n+1)]
    ret = True
    for y in range(1, n+1) :
        if visited[y] == 'N' :
            ret = bfs(line, visited, y)
        if ret is False :
            break
    if ret is True :
        print('YES')
    else :
        print('NO')


