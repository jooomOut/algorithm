word = input()

data = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
temp = []
for i in range(len(word)) :
    if word[i].islower() and not word[i].isnumeric() :
        idx = data.index(word[i])
        temp.append(data[(idx+13)%26])
    elif word[i].isupper()  and not word[i].isnumeric() :
        tempWord = word[i].lower()
        idx = data.index(tempWord)
        tempWord = data[(idx+13)%26]
        temp.append(tempWord.upper())
    else :
        temp.append(word[i])
for i in range(len(temp)) :
    print(temp[i], end='')