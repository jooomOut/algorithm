from collections import deque
import sys
sys.setrecursionlimit(1000000)

def dfs(data, here, visited, stack) :
    visited[here] = True
    for x in data[here] :
        if visited[x] is False and stack.__contains__(x) is False:
            stack.append(x)
    if stack :
        dfs(data,stack.pop(),visited, stack)

def bfs(data, here, visited) :
    q = deque()
    q.append(here)
    while q :
        here = q.popleft()
        visited[here] = True
        for x in data[here]:
            if visited[x] is False and q.__contains__(x) is False:
                q.append(x)


n , m = map(int, sys.stdin.readline().split())

data = [[] for _ in range(n+1)]
visited = [False for _ in range(n+1)]
for i in range(m) :
    u,v = map(int, sys.stdin.readline().split())
    data[u].append(v)
    data[v].append(u)

result = 0
for i in range(1,n+1) :
    if visited[i] is False :
        result = result + 1
        bfs(data, i, visited)

print(result)