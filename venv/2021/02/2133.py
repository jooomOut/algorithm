n = int(input())

dp = [ 0 for _ in range(n+1)]
result = 0
if n % 2 == 0 :
    dp[0] = 1
    dp[2] = 3

    for i in range(4, n+1) :
        if i % 2 == 1:
            continue
        for j in range(2, i+1) :
            if j % 2 == 1:
                continue
            mul = 2
            if j == 2:
                mul = 3
            dp[i] = dp[i] + mul * dp[i-j]
    result = dp[n]

print(result)