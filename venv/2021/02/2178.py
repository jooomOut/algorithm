import sys
from collections import deque

def bfs(data, x, y, maxY, maxX) :
    q = deque()
    if x== maxX-1 and y == maxY-1 :
        return data[y][x]
    #1차 세팅
    q.append([0,0])
    while q:
        x,y = q.popleft()
        for i in range(4):
            nx = x + dx[i]
            ny = y + dy[i]
            if 0<= nx < maxX and 0<= ny < maxY :
                if data[ny][nx] == 1:
                    q.append([nx,ny])
                    data[ny][nx] = data[y][x] + 1

    return data[maxY-1][maxX-1]



maxCol, maxRow = map(int,input().split())
data = []
for p in range(maxCol) :
    sen = input()
    temp = []
    for o in range(len(sen)) :
        temp.append(int(sen[o]))
    data.append(temp)

dx = [0, 0, 1, -1]
dy = [1,-1, 0,  0]
trace = []
print(bfs(data, 0, 0, maxCol, maxRow))

