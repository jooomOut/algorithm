tc = int(input())

for i in range(tc) :
    data = input()
    check = 0
    for j in range(len(data)) :
        if data[j] == '(' :
            check = check + 1
        else :
            check = check - 1
        if check < 0 :
            print("NO")
            break
    if check == 0 :
        print("YES")
    elif check > 0 :
        print("NO")