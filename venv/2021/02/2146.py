from collections import deque
import sys

def setting(data, n) :
    q = deque()
    label = 0
    for p in range(n):
        for o in range(n) :
            if data[p][o] == 1 :
                label = label - 1
                q.append([p,o])
                data[p][o] = label
                while q :
                    y, x = q.popleft()
                    data[y][x] = label
                    for i in range(4) :
                        ny = y + dy[i]
                        nx = x + dx[i]
                        if 0 <= ny < n and 0 <= nx < n :
                            if data[ny][nx] == 1 and [ny,nx] not in q:
                                q.append([ny,nx])
                            elif data[ny][nx] == 0 and [y,x] not in beach:
                                beach.append([y,x])
def bfs(data, n) :
    loop = 0
    ans  = sys.maxsize
    while beach :
        loop = loop + 1
        length = len(beach)
        for _ in range(length) :
            y,x = beach.popleft()
            for i in range(4):
                ny = y + dy[i]
                nx = x + dx[i]
                if 0 <= ny < n and 0 <= nx < n:
                    if data[ny][nx] == 0:
                        beach.append([ny,nx])
                        data[ny][nx] = data[y][x]
                        continue
                    elif data[ny][nx] < 0:
                        if data[y][x] == data[ny][nx] :
                            continue
                        elif data[y][x] < data[ny][nx] :
                            #print('data[',y,'][',x,']==',data[y][x],' <<<', 'data[',ny,'][',nx,']==', data[ny][nx])
                            ans = min(ans,loop * 2-1)
                        elif data[y][x] > data[ny][nx] :
                            #print('data[', y, '][', x, ']==', data[y][x], ' >>>, data[', ny, '][', nx, '],==', data[ny][nx])
                            ans = min(ans, (loop-1)*2)
    return ans

n = int(input())
data = []
for p in range(n) :
    data.append(list(map(int,input().split())))
dx = [0,0,1,-1]
dy = [1,-1,0,0]
beach = deque()
setting(data, n)


print(bfs(data, n))

