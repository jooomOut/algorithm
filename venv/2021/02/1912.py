import sys
size = int(sys.stdin.readline())
data = list(map(int, sys.stdin.readline().split()))

dp = [-1000 for _ in range(size)]
dp[0] = data[0]

for i in range(len(data)) :
    dp[i] = max(data[i], dp[i-1]+data[i])

print(max(dp))