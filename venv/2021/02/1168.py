from queue import Queue
import bisect
import sys

class fenwick() :
    def __init__(self, sizeN):
        self.data = [0] + [0 for _ in range(sizeN)]
        self.sizeN = sizeN

    def subSum(self, start, end):
        # end - start
        endSum = 0
        while end != 0 :
            endSum = endSum + self.data[end]
            end = end & (end - 1)
        startSum = 0
        while start != 0 :
            startSum = startSum + self.data[start]
            start = start & (start - 1)

        return endSum - startSum

    def update(self, idx, value):
        if idx > self.sizeN :
            self.data.append(0)
            self.sizeN = self.sizeN+1

        while idx <= self.sizeN :
            self.data[idx] = self.data[idx] + value
            idx = idx + (idx & -idx)


    def biSearch(self, value) :
        left = 1
        right = self.sizeN
        ret = 1
        while left <= right :
            mid = (left + right) >> 1
            if self.subSum(0,mid) >= value  :
                ret = mid
                right = mid-1
            else :
                left = mid+1
        return ret



n, k = map(int,input().split())

result = []

fen = fenwick(n)
arr= [0]
for i in range(1,n+1):
    arr.append(i)
    fen.update(i,1)

sys.stdout.write("<")
value = k
for i in range(n, 0, -1) :
    if value % i > 0 :
        value = value % i
    else :
        value = i

    target = fen.biSearch(value)
    sys.stdout.write(str(target))
    if i != 1:
        sys.stdout.write(', ')
    fen.update(target,-1)
    arr[target] = arr[target]-1
    value = value + k - 1

sys.stdout.write(">")





