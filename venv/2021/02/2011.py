import sys
sys.setrecursionlimit(100000)

def solve(word, idx, last, dp) :
    if dp[idx] != -1 :
        return dp[idx]
    # end

    try :
        if int(word[idx]) == 0 :
            return 0
    except :
        if idx >= last:
            return 1
    if idx >= last :
        return 1


    if int(word[idx]) == 1 or int(word[idx]) == 2 : #두 자리의 가능성
        one = 0
        if int(word[idx+1]) != 0 : # 다음자리가 0이면 한자리수 불가능, 실패한 경로
            one = solve(word, idx+1 , last, dp) % 1000000

        two = 0
        if (int(word[idx]) == 2 and int(word[idx+1]) < 7) or int(word[idx]) == 1  : #두 자리 ㅇㅋ
            two = solve(word, idx+2 , last, dp)
        dp[idx] = (one + two) % 1000000
        return dp[idx]
    else : # 무적권 한자리수
        one = 0
        if int(word[idx + 1]) != 0:  # 다음자리가 0이면 한자리수 불가능, 실패한 경로
            one = solve(word, idx + 1, last, dp) % 1000000
        dp[idx] = one
        return dp[idx]




word = input()
# 0 ~ len(word)-1 이 실제 인덱스
#dp = [[-1,-1,-1] for _ in range(len(word+1))]
dp = [-1 for _ in range(len(word)+1)]
print(solve(word, 0,len(word)-1, dp))

