#push X: 정수 X를 스택에 넣는 연산이다.
#pop: 스택에서 가장 위에 있는 정수를 빼고, 그 수를 출력한다. 만약 스택에 들어있는 정수가 없는 경우에는 -1을 출력한다.
#size: 스택에 들어있는 정수의 개수를 출력한다.
#empty: 스택이 비어있으면 1, 아니면 0을 출력한다.
#top: 스택의 가장 위에 있는 정수를 출력한다. 만약 스택에 들어있는 정수가 없는 경우에는 -1을 출력한다.
import sys

class Stack :
    def __init__(self) :
        self.li = []
        self.si = 0

    def push(self,x):
        self.li.append(x)
        self.si = self.si+1

    def size(self):
        print(self.si)
    def empty(self):
        if self.si == 0 :
            print(1)
        else :
            print(0)
    def pop(self):
        if self.si == 0 :
            print(-1)
        else :
            print(self.li.pop(self.si-1))
            self.si = self.si -1
    def top(self):
        if self.si == 0:
            print(-1)
        else:
            print(self.li[self.si - 1])



stack = Stack()

n = int(sys.stdin.readline())
for i in range(n) :
    data = list(sys.stdin.readline().split())

    if data[0] == 'push' :
        stack.push(int(data[1]))
    elif data[0] == 'size' :
        stack.size()
    elif data[0] == 'empty' :
        stack.empty()
    elif data[0] == 'pop' :
        stack.pop()
    else :
        stack.top()
