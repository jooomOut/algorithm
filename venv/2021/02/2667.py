def dfs(data , col, row, stack):
    data[col][row] = -1
    result = 1
    try :
        if col >= 1 and data[col-1][row] != 0 and data[col-1][row] != -1 :
            result = result + dfs(data, col-1,row , stack)
    except :
        pass
    try:
        if col < len(data[col]) and data[col+1][row] != 0 and data[col+1][row] != -1 :
            result = result + dfs(data, col+1, row, stack)
    except :
        pass
    try:
        if row >= 1 and data[col][row-1] != 0 and data[col][row-1] != -1 :
            result = result + dfs(data,col,row-1, stack)
    except :
        pass

    try:
        if row < len(data[col]) and data[col][row+1] != 0 and data[col][row+1] != -1 :
            result = result + dfs(data, col,row+1,stack)
    except :
        pass

    return result


n = int(input())
data = []
for i in range(n) :
    temp = []
    sen = input()
    for j in range(len(sen)) :
        temp.append(int(sen[j]))
    data.append(temp)

result = []
num = 0
for i in range(n):
    for j in range(n):
        if data[i][j] == 0 or data[i][j] == -1:
            continue
        else :
            # travel
            stack = []
            result.append(dfs(data, i, j, stack))

            num = num + 1

print(num)
result.sort()
for x in result :
    print(x)