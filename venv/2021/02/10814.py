import sys
n = int(input())
data = []
for i in range(n) :
    [a, b] = sys.stdin.readline().split()
    data.append([int(a), b])

data.sort(key=lambda x :x[0])

for i in range(n) :
    print(data[i][0],data[i][1])