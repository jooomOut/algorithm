n, m = map(int, input().split())

tree = list(map(int,input().split()))
tree.sort(reverse=True)

right = tree[0]
left = 1
result = 0
while left <= right :
    mid = (left + right) // 2
    count = 0
    for x in tree :
        if x - mid < 0 :
            count = count
        else :
            count = count + x - mid

    if count >= m :
        left = mid + 1
        result = mid
    else :
        right = mid - 1


print(result)