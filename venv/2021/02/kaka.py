def solution(board, moves):
    answer = 0
    stack = []
    lastIdx = -1
    for i in range(len(moves)):
        targetCol = moves[i] - 1
        for j in range(len(board[0])):
            if board[j][targetCol] != 0:
                if lastIdx != -1:
                    if board[j][targetCol] == stack[lastIdx]:
                        answer = answer + 2
                        stack.pop(lastIdx)
                        lastIdx = lastIdx - 1

                    else:
                        stack.append(board[j][targetCol])
                        lastIdx = lastIdx + 1

                else:
                    stack.append(board[j][targetCol])
                    lastIdx = lastIdx + 1

                board[j][targetCol] = 0
                break

    return answer