import sys
size = int(input())
data = list(map(int, sys.stdin.readline().split()))

dpUp = [ 1 for _ in range(size+1)]
dpDown = [ 1 for _ in range(size+1)]

for i in range(size) :
    for j in range(0,i+1):
        if data[j] < data[i] :
            dpUp[i] = max(dpUp[i], dpUp[j]+1)
for i in range(size-1,-1, -1):
    for j in range(i, size):
        if data[i] > data[j] :
            dpDown[i] = max(dpDown[i], dpDown[j]+1)

dp = []

for i in range(len(dpUp)) :
    dp.append(dpUp[i]+dpDown[i])

print(max(dp)-1)