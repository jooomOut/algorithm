import math

dp = [1 for _ in range(1000001)]

mid = int(math.sqrt(1000000))

for i in range(2, mid + 1):
    j = 0
    if dp[i] == 1:
        j = i + i
        while j <= 1000000:  # n
            dp[j] = 0
            j = j + i
# ==========================

while True :
    x = int(input())
    if x != 0 :


        a = 2
        b = x
        while a <= b: # n
            b = x - a
            if dp[b] == 1:
                print(x, '=', a, '+', b)
                break
            a = a + 1
            while dp[a] != 1:
                a = a + 1
        if a > b:
            print("Goldbach's conjecture is wrong.")

    else :
        break
