N = input()

N = list(N)
answer = -1

if min(N) == '0':
    max_num = sorted(N, reverse=True)
    max_num = int(''.join(max_num))

    if max_num % 3 == 0:
        answer = max_num

print(answer)
