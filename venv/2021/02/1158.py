n, k = map(int,input().split())
data = []
result = []
for i in range(1, n+1) :
    data.append(i)
idx = 0

while len(data) != 0 :
    idx = idx + k -1
    idx = idx % len(data)
    result.append(data[idx])

print('<',end='')
for i in range(len(result)) :
    if i != 0 :
        print(',',result[i],end='')
    else :
        print(result[i],end='')
print('>')


