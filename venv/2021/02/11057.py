def solve(num, left, cache) :
    if cache[left][num] != -1 :
        return cache[left][num]

    if left <= 1 :
        return 1

    ret = 0
    for i in range(num, 10) :
        ret = ret + solve(i, left-1, cache)

    cache[left][num] = ret % 10007
    return cache[left][num]

n = int(input())

cache = [[-1 for _ in range(10)] for _ in range(n+1)]
result = 0
for i in range(0,10) :
    result = result + solve(i, n, cache)

print(result%10007)
