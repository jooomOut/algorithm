import sys
sys.setrecursionlimit(1000000)
def mergeSort(data, start, end) :
    if start == end :
        return

    mid = int((start + end)/2)

    mergeSort(data, start, mid)
    mergeSort(data, mid+1, end)

    merge(data, start, mid, end)

def merge(data, start, mid, end) :
    sortedList = []
    i = start
    j = mid + 1

    while i <= mid and j <= end :
        if data[i] <= data[j] :
            sortedList.append(data[i])
            i = i + 1
        else :
            sortedList.append(data[j])
            j = j + 1
    if i > mid :
        for y in range(j, end + 1) :
            sortedList.append(data[y])
    elif j > end :
        for y in range(i, end + 1) :
            sortedList.append(data[y])

    for i in range(start, end + 1) :
        data[i] = sortedList[i-start]

n = int(input())

data = []
for i in range(n) :
    data.append(int(sys.stdin.readline()))

mergeSort(data, 0, n-1)
for i in range(n) :
    print(data[i])
