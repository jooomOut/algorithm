import sys
size = int(input())
data = list(map(int, sys.stdin.readline().split()))


dp = data[:]

for i in range(len(data)) :
    #maxDP = 0
    for j in range(0, i+1) :
        if data[j] < data[i] :
            dp[i] = max(dp[j]+data[i], dp[i])
            #maxDP = max(maxDP, dp[i])
print(max(dp))
