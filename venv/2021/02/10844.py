def solve(num, left, cache) :
    if cache[left][num] != -1 :
        return cache[left][num]
    if left <= 1 :
        return 1
    up = num + 1
    down = num - 1
    if up == 10 :
        cache[left][num] = solve(down, left-1,cache) % 1000000000
        return cache[left][num]
    elif down == -1 :
        cache[left][num] = solve(up, left - 1, cache) % 1000000000
        return cache[left][num]
    else :
        cache[left][num] = (solve(down, left-1, cache) + solve(up, left-1, cache)) % 1000000000

    return cache[left][num]


n = int(input())
result = 0

cache = [[-1 for _ in range(10)] for _ in range(n+1)]

#print(cache)
for i in range(1,10) :
    #print('solve(',i, ',',  n , ',', 'cache', ') = ', solve(i, n, cache))
print(result%1000000000)