import math
m, n = map(int,input().split())

data = [1 for _ in range(n+1)]
data[0] = 0
data[1] = 0
sq = int(math.sqrt(n))

for i in range(2, sq+1) :
    if data[i] == 1:
        j = i*2
        while j <= n :
            data[j] = 0
            j = j+i

for i in range(m,n+1):
    if data[i] == 1 :
        print(i)
