tc = int(input())

for i in range(0, tc) :
    data =[]
    column = int(input())

    dp = [[0 for _ in range(column+1)] for _ in range(2)]

    for j in range(0, 2) :
        data.append(list(map(int, input().split())))


    dp[0][0] = dp[1][0] = 0
    dp[0][1] = data[0][0]
    dp[1][1] = data[1][0]

    for z in range(2,column+1) :
        dp[0][z] = data[0][z-1] + max(dp[1][z-1], dp[1][z-2])
        dp[1][z] = data[1][z-1] + max(dp[0][z - 1], dp[0][z - 2])

    print(max(dp[0][column], dp[1][column]))

