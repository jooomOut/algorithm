def solve(n, cache) :
    if cache[n] != -1 :
        return cache[n]
    if n == 1 :
        return 1
    if n <= 0 :
        return 0

    cache[n] = solve(n-1, cache) + solve(n-2, cache) + solve(n-3, cache)
    if n <= 3 :
        cache[n] = cache[n] + 1
    return cache[n]


tc = int(input())

for _ in range(tc) :
    n = int(input())
    cache = [-1 for _ in range(n+1)]
    print(solve(n,cache)) # 마지막에 본인 빼주기