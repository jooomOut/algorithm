import  sys

size = int(sys.stdin.readline())

data = list(map(int, sys.stdin.readline().split()))

dp = [1 for _ in range(size+1)]

for i in range(size) :
    for j in range(0,i+1) :
        if data[i] < data[j] :
            dp[i] = max(dp[i], dp[j]+1)

print(max(dp))
