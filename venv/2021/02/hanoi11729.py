from collections import deque
n = int(input())

def move(a,b, n, q) :
    c = 0
    if n == 1:
        q.append(str(a)+' '+str(b))
        c = c + 1
    else :
        stack = []
        next = 6 - a - b

        c= c +move(a,next,n-1,q)
        q.append(str(a) + ' ' + str(b))
        c = c + 1
        c = c +move(next, b, n - 1, q)
    return c

q = deque()
print(move(1,3,n, q))
while q :
    print(q.popleft())