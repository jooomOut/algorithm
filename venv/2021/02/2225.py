def solve(num, left, dp) :
    if dp[left][num] != -1 :
        return dp[left][num]
    if left == 1 :
        dp[left][num] = 1
        return dp[left][num]
    count = 0
    for i in range(0, num+1) :
        count = count + solve(num-i, left-1, dp)
    dp[left][num] = count % 1000000000
    return dp[left][num]

n, k = map(int, input().split())

dp = [[-1 for _ in range(n+1)] for _ in range(k + 1)]

print(solve(n, k, dp))