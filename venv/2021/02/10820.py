import sys

for word in sys.stdin :
    result = [0,0,0,0]
    for i in range(len(word)) :
        if word[i].islower() :
            result[0] = result[0] + 1
        elif word[i].isupper() :
            result[1] = result[1] + 1
        elif word[i].isnumeric() :
            result[2] = result[2] + 1
        elif word[i].isspace() :
            result[3] = result[3] + 1
    result[3] = result[3] -1
    for j in range(0,4):
        print(result[j], end=' ')
