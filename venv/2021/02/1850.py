import sys

def gcd(a, b) :
    while b!= 0 :
        r = a%b
        a = b
        b = r
    return a

a, b = map(int,input().split())

if a < b :
    a , b = b , a

result = gcd(a,b)

for j in range(result):
    sys.stdout.write('1')

