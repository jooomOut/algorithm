tc = int(input())
for x in range(tc) :
    n = int(input())

    temp = [1,0,0,1,0]
    dp = [0 for _ in range(n)]
    dp = temp + dp

    for i in range(5, len(dp)) :
        dp[i] = dp[i-1] + dp[i-5]

    print(max(dp))