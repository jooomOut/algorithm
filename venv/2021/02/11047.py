n, k = map(int, input().split())
data = []
for i in range(n) :
    data.append(int(input()))

count = 0
i = len(data)-1
while i >= 0 :
    count = count + (k // data[i])
    k = k%data[i]
    i = i - 1

print(count)

