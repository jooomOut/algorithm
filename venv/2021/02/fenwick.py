class fenwick() :
    def __init__(self):
        self.arr = []
        self.data = []
        self.sizeN = 0

    def set(self, arr):
        self.arr = [0]+arr
        self.sizeN = len(arr)
        i = 0
        while 2**i < self.sizeN :
            i = i+ 1
        self.data = [0 for _ in range(2**i + 1)]

        for i in range(1, self.sizeN+1) :
            if i % 2 == 1:
                self.data[i] = self.arr[i]
            elif i % 2 == 0 and i % 4 != 0 :
                self.data[i] = self.arr[i] + self.arr[i-1]
            else :
                for k in range(self.sizeN) :
                    if i % 2**k == 0 and i % 2**(k+1) != 0 :
                        for j in range(i, i-2**k-1, -1) :
                            self.data[i] = self.data[i] + self.arr[j]

    def subSum(self, start, end):
        # end - start
        endSum = 0
        while end != 0 :
            endSum = endSum + self.data[end]
            end = end & (end - 1)
        startSum = 0
        while start != 0 :
            startSum = startSum + self.data[start]
            start = start & (start - 1)

        return endSum - startSum

    def update(self, i, value):
        self.arr[i] = value
        while i <= self.sizeN :
            if i % 2 == 1:
                self.data[i] = self.arr[i]
            elif i % 2 == 0 and i % 4 != 0 :
                self.data[i] = self.arr[i] + self.arr[i-1]
            else :
                for k in range(self.sizeN) :
                    if i % 2**k == 0 and i % 2**(k+1) != 0 :
                        self.data[i] = 0
                        for j in range(i, i-2**k-1, -1) :
                            self.data[i] = self.data[i] + self.arr[j]
                        break
            i = i*2 - (i & (i-1))