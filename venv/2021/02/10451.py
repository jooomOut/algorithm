tc = int(input())

for t in range(tc) :
    n = int(input())
    data = [0] + list(map(int, input().split()))
    visited = [False for _ in range(n+1)]

    cnt = 0

    for i in range(1, n+1) :
        if visited[i] is False :
            idx = i
            while visited[idx] != True :
                visited[idx] = True
                idx = data[idx]
            cnt = cnt + 1

    print(cnt)
