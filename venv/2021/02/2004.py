n , m = map(int, input().split())

def by5(num) :
    ret = 0
    i = 5
    while i <= num :
        ret = ret + num // i
        i = i*5
    return ret

def by2(num) :
    ret = 0
    i = 2
    while i <= num:
        ret = ret + num // i
        i = i * 2
    return ret

on5 = by5(n) - (by5(n-m) + by5(m))
on2 = by2(n) - (by2(n-m) + by2(m))

print(min(on5,on2))

