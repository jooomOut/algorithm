from collections import deque


row, col = map(int,input().split())
data = []
for p in range(col):
    data.append(list(map(int,input().split())))

days = 0
cnt = 0
aq = deque()
bq = deque()
for i in range(col) :
    for j in range(row) :
        if data[i][j] == 1 :
            aq.append([i,j])
useA = True
while aq or bq :
    if useA :
        while aq :
            cr = aq.popleft()
            if cr[0]-1 >=0 and data[cr[0]-1][cr[1]] == 0 :
                data[cr[0] - 1][cr[1]] = 1
                bq.append([cr[0]-1, cr[1]])

            if cr[0]+1 < col and data[cr[0]+1][cr[1]] == 0 :
                data[cr[0] + 1][cr[1]] = 1
                bq.append([cr[0]+1, cr[1]])

            if cr[1]-1 >=0 and data[cr[0]][cr[1]-1] == 0 :
                data[cr[0]][cr[1]-1] = 1
                bq.append([ cr[0] , cr[1]-1])

            if cr[1]+1 < row and data[cr[0]][cr[1]+1] == 0 :
                data[cr[0]][cr[1]+1] = 1
                bq.append([cr[0], cr[1]+1])

        useA = False
    else :
        while bq :
            cr = bq.popleft()
            if cr[0]-1 >=0 and data[cr[0]-1][cr[1]] == 0 :
                data[cr[0] - 1][cr[1]] = 1
                aq.append([cr[0]-1, cr[1]])

            if cr[0]+1 < col and data[cr[0]+1][cr[1]] == 0 :
                data[cr[0] + 1][cr[1]] = 1
                aq.append([cr[0]+1, cr[1]])

            if cr[1]-1 >=0 and data[cr[0]][cr[1]-1] == 0 :
                data[cr[0]][cr[1]-1] = 1
                aq.append([ cr[0] , cr[1]-1])

            if cr[1]+1 < row and data[cr[0]][cr[1]+1] == 0 :
                data[cr[0]][cr[1]+1] = 1
                aq.append([cr[0], cr[1]+1])
        useA = True
    days = days + 1

count = 0
for i in range(col) :
    for j in range(row) :
        if data[i][j] == 0 :
            print(-1)
            exit()

print(days-1)




