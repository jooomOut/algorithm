n = int(input())
#n = 6
switchList = list(map(int, input().split()))
#switchList = [2,1,3,5,6,4]
k = int(input())
#k = 123
start = 0
answer = False


for i in range(0, 2**n) :
    retry = True
    binNum = str(bin(i))[2:]
    binNum =(n-len(binNum)) * '0' + binNum

    #print(binNum)
    turn = True
    temp = 0
    for j in range(0,n) :
        if binNum[j] == '1' :
            if switchList[j] < temp :
                turn= False
                retry = False
                break
            temp = switchList[j]
        if retry == False :
            break

    if turn :
        #print(start)
        #print(int('0b' + binNum, 2))
        start = start + 1
        if start == k :
            print(int('0b'+binNum,2))
            answer = True
            break;



if start < k and answer == False :
    print(-1)




