def getNext(n, mul) :
    ret = 0
    while n != 0 :
        temp = 0
        q = n % 10
        ret = ret + q**mul
        n = n //10
    return ret

a,p = map(int,input().split())

#visited = [0 for _ in range(500000)]
visited = {}
dic = {}


dic[0] = a
key = 0
visited[key] = 0
while True :
    key = dic[key]
    if visited.get(key) is None :
        visited[key] = 1
    elif visited[key] == 1 :
        visited[key] = visited[key] + 1
        break
    else :
        break
    #next
    dic[key] = getNext(key, p)
key = dic[0]
count= 0
while visited[key] < 2 :
    count = count + 1
    key = dic[key]

print(count)


