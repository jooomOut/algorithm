def check (data, row, col, n, find) :
    ret = 0
    for i in range(row,row+n) :
        for j in range(col,col+n) :
            if data[i][j] == find :
                ret = ret + 1
    if ret == n*n :
        return 1
    else :
        return 0

def solve(data, row, col, n, find) :
    if check(data, row, col, n, find) == 1 :
        return 1
    elif n //3 >= 1 :
        count = 0
        for i in range(1,4) :
            for j in range(1,4) :
                count = count + solve(data, row+(i-1)*(n//3), col+(j-1)*(n//3), n//3  ,find)

        return count
    else :
        return 0




n = int(input())
data = []
data.append([0])
for i in range(n) :
    data.append([0]+ list(map(int, input().split())))

print(solve(data, 1, 1, n,-1))
print(solve(data, 1, 1, n, 0))
print(solve(data, 1, 1, n, 1))

