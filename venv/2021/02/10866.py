"""
push_front X: 정수 X를 덱의 앞에 넣는다.
push_back X: 정수 X를 덱의 뒤에 넣는다.
pop_front: 덱의 가장 앞에 있는 수를 빼고, 그 수를 출력한다. 만약, 덱에 들어있는 정수가 없는 경우에는 -1을 출력한다.
pop_back: 덱의 가장 뒤에 있는 수를 빼고, 그 수를 출력한다. 만약, 덱에 들어있는 정수가 없는 경우에는 -1을 출력한다.
size: 덱에 들어있는 정수의 개수를 출력한다.
empty: 덱이 비어있으면 1을, 아니면 0을 출력한다.
front: 덱의 가장 앞에 있는 정수를 출력한다. 만약 덱에 들어있는 정수가 없는 경우에는 -1을 출력한다.
back: 덱의 가장 뒤에 있는 정수를 출력한다. 만약 덱에 들어있는 정수가 없는 경우에는 -1을 출력한다.
"""
import sys

class Deque :
    def __init__(self) :
        self.li = []
        self.si = 0

    def push_front(self,x):
        self.li.insert(0,x)
        self.si = self.si+1
    def push_back(self,x):
        self.li.append(x)
        self.si = self.si+1
    def size(self):
        print(self.si)
    def empty(self):
        if self.si == 0 :
            print(1)
        else :
            print(0)
    def pop_front(self):
        if self.si == 0 :
            print(-1)
        else :
            print(self.li.pop(0))
            self.si = self.si -1
    def pop_back(self):
        if self.si == 0 :
            print(-1)
        else :
            print(self.li.pop(self.si-1))
            self.si = self.si -1
    def front(self):
        if self.si == 0:
            print(-1)
        else:
            print(self.li[0])
    def back(self):
        if self.si == 0:
            print(-1)
        else:
            print(self.li[self.si - 1])


queue = Deque()

n = int(sys.stdin.readline())
for i in range(n) :
    data = list(sys.stdin.readline().split())

    if data[0] == 'push_front' :
        queue.push_front(int(data[1]))
    elif data[0] == 'push_back' :
        queue.push_back(int(data[1]))
    elif data[0] == 'size' :
        queue.size()
    elif data[0] == 'empty' :
        queue.empty()
    elif data[0] == 'pop_front' :
        queue.pop_front()
    elif data[0] == 'pop_back' :
        queue.pop_back()
    elif data[0] == 'front' :
        queue.front()
    else :
        queue.back()