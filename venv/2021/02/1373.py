import  sys
b = '000' + str(sys.stdin.readline().strip())

data= ''

for i in range(len(b),2,-3) :
    cut = str(b[int(i) - 3:int(i)])

    temp = 0
    for j in range(2,-1,-1) :
        if cut[j] == '1' :
            temp = 2**(2-j) + temp
    data = str(temp) + data

sys.stdout.write(data)