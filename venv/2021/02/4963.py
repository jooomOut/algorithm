import sys
sys.setrecursionlimit(1000000)

def dfs(data , col, row, maxCol, maxRow):
    data[col][row] = -1

    for i in range(col-1, col+2) :
        for j in range(row-1, row+2) :
            if i>= 0 and i <= maxCol-1 and j >= 0 and j <= maxRow-1 :
                if data[i][j] != 0 and data[i][j] != -1 :
                    dfs(data, i, j, maxCol, maxRow)

while True :
    maxRow, maxCol = map(int, input().split())
    if maxRow == 0 and maxCol == 0 :
        break
    data = []
    for q in range(maxCol) :
        data.append(list(map(int,input().split())))

    num = 0
    for i in range(maxCol):
        for j in range(maxRow):
            if data[i][j] == 0 or data[i][j] == -1:
                continue
            else:
                # travel
                dfs(data, i, j, maxCol, maxRow)
                num = num + 1
    sys.stdout.write(num)

