tc = int(input())

for tx in range(tc) :
    n = int(input())
    students = [0] + list(map(int, input().split()))
    visited = [False] + [False for _ in range(n)]
    cnt = 0
    for x in range(1,len(students)) :
        if x == 0 or visited[x] is True:
            continue
        if students[x] == x :
            visited[x] = True
            continue
        cache = []
        idx = x

        while True :
            if visited[idx] is False and students[idx] != idx:
                visited[idx] = True
                cache.append(idx)
                idx = students[idx]

            else :
                if idx == x :
                    pass
                elif idx in cache :
                    cnt = cnt + (cache.index(idx))
                else :
                    cnt = cnt + len(cache)
                    #for y in cache :
                    #    visited[y] = False
                break

#    count = 0
#    for x in visited :
 #       if x is False :
   #         count = count + 1
    print(cnt)




