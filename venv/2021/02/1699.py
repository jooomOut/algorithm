num = int(input())

dp = [0]
for i in range(1, num+1) :
    dp.append(i)

for i in range(num+1) :
    for j in range(i):
        if j*j > i :
            break;
        dp[i] = min(dp[i], 1 + dp[i-j*j])



print(dp[num])