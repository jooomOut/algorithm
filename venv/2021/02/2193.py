def solve(left, beforeIsOne, cache) :

    if left <= 1 :
        return 1

    if beforeIsOne :
        if cache[left][1] != -1 :
            return cache[left][1]
        cache[left][1] = solve(left-1, False, cache)
        return cache[left][1]
    else :
        if cache[left][0] != -1 :
            return cache[left][0]
        cache[left][0] = solve(left-1, False, cache) + solve(left-1, True, cache)
        return cache[left][0]

n = int(input())
cache = [[-1,-1] for _ in range(n+1)]
print(solve(n, True, cache))
