import sys
size = int(input())
data = list(map(int, sys.stdin.readline().split()))

dp = [ 1 for _ in range(size+1)]

for i in range(len(data)) :
    #maxDP = 0
    for j in range(0, i) :
        if data[j] < data[i] :
            dp[i] = max(dp[j]+1, dp[i])
            #maxDP = max(maxDP, dp[i])
print(max(dp))
