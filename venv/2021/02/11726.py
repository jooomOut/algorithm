def solve(n, cache) :
    if cache[n] != -1 :
        return cache[n]

    if n <= 1 :
        return 1
    else :
        cache[n] = (solve(n-1, cache) + solve(n-2, cache)) % 10007
        return cache[n]


n = int(input())
cache = [-1 for _ in range(n+1)]
#print(cache)
print(solve(n, cache))