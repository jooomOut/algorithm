import sys
left = list(sys.stdin.readline().strip())
tc = int(sys.stdin.readline())

right =[]
for i in range(tc) :

    ins = sys.stdin.readline().strip()
    if ins == 'L' :
        if len(left) != 0 :
            right.append(left.pop())
    elif ins == "D" :
        if len(right) != 0:
            left.append(right.pop())
    elif ins == "B" :
        if len(left) != 0:
            left.pop()
    else :
        spIns = list(map(str, ins.split()))
        left.append(spIns[1])

for i in range(len(left)) :
    print(left[i], end='')
for i in range(len(right)-1, -1, -1) :
    print(right[i], end='')