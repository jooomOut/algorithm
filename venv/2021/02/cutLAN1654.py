import sys
sys.setrecursionlimit(100000)
k,n = map(int, input().split())
data = []
for i in range(k) :
    data.append(int(input()))

def bisect(left, right, data, n) :
    result = 0

    while left <= right :
        mid = (left + right) // 2
        cnt = 0
        for i in range(len(data)) :
            cnt = cnt + data[i] // mid

        if cnt < n :
            right = mid - 1
        else :
            left = mid+1
            result = mid

    print(result)


data.sort(reverse=True)
bisect(1,data[0], data, n)

