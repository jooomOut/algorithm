import sys
def solve(data, left, dp) :
    if dp[left] != -1 :
        return dp[left]
    if left == 0 :
        return 0
    result = 0
    for i in range(1, left+1) :
        result = max(result,
                     solve(data, left-i, dp) + data[i])
    dp[left] = result
    return dp[left]

n = int(input())
data = [0] + list(map(int, input().split()))
dp = [-1 for _ in range(n+1)]

print(solve(data, n, dp))
