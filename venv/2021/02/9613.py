def gcd(a,b) :
    while b!= 0 :
        r = a%b
        a = b
        b = r
    return a



tc = int(input())

for t in range(tc) :
    result = 0
    data = list(map(int,input().split()))

    for i in range(1, data[0]) :
        for j in range(i+1, data[0]+1) :
            if i >= j :
                result = result + gcd(data[i], data[j])
            else :
                result = result + gcd(data[j], data[i])
    print(result)

