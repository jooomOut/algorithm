#push X: 정수 X를 큐에 넣는 연산이다.
#pop: 큐에서 가장 앞에 있는 정수를 빼고, 그 수를 출력한다. 만약 큐에 들어있는 정수가 없는 경우에는 -1을 출력한다.
#size: 큐에 들어있는 정수의 개수를 출력한다.
#empty: 큐가 비어있으면 1, 아니면 0을 출력한다.
#front: 큐의 가장 앞에 있는 정수를 출력한다. 만약 큐에 들어있는 정수가 없는 경우에는 -1을 출력한다.
#back: 큐의 가장 뒤에 있는 정수를 출력한다. 만약 큐에 들어있는 정수가 없는 경우에는 -1을 출력한다.
import sys

class Queue :
    def __init__(self) :
        self.li = []
        self.si = 0

    def push(self,x):
        self.li.append(x)
        self.si = self.si+1
    def size(self):
        print(self.si)
    def empty(self):
        if self.si == 0 :
            print(1)
        else :
            print(0)
    def pop(self):
        if self.si == 0 :
            print(-1)
        else :
            print(self.li.pop(0))
            self.si = self.si -1
    def front(self):
        if self.si == 0:
            print(-1)
        else:
            print(self.li[0])
    def back(self):
        if self.si == 0:
            print(-1)
        else:
            print(self.li[self.si - 1])


queue = Queue()

n = int(sys.stdin.readline())
for i in range(n) :
    data = list(sys.stdin.readline().split())

    if data[0] == 'push' :
        queue.push(int(data[1]))
    elif data[0] == 'size' :
        queue.size()
    elif data[0] == 'empty' :
        queue.empty()
    elif data[0] == 'pop' :
        queue.pop()
    elif data[0] == 'front' :
        queue.front() 
    else :
        queue.back()
    
