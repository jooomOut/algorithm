string = input()
bomb = input()

stack = []
lastBomb = bomb[-1]
bombLen = len(bomb)

for x in string:
    stack.append(x)
    if stack[-1] == lastBomb:
        if ''.join(stack[-bombLen:]) == bomb:
            del stack[-bombLen:]

ret = ''.join(stack)

if len(ret) == 0:
    print('FRULA')
else:
    print(ret)
